Init environment :

    - Create virtual env

        $ virtualenv path_to_your_virtual_env/FarmOsEnv

    - Activate virtual env

        $ source path_to_your_virtual_env/FarmOsEnv/bin/activate

    - Install dependencies :

        $ cd FarmOS

     * Python :

        $ pip install -r requirements.txt

        in case you add a new requirement using pip install please execute this command:

        $ pip freeze > requirements.txt

     * Javascript :

        $ python manage.py bower install

        in case you install a new bower component using "python manage.py bower install angular" please execute this command:

        $ python manage.py bower freeze

        then replace BOWER_INSTALLED_APPS in "(root_path)/FarmOS/setting.py" with the output

    - Init Database :

      Create 'farmos' database

        CREATE DATABASE farmos;

      You can change database access parameters by changing POSTGRES in "(root_path)/FarmOS/setting_dev.py" or "(root_path)/FarmOS/setting_prod.py"

      in case you modified the models "core/models.py" execute :

        $ python manage.py makemigrations


      * Then init your DB :

        $ python manage.py migrate

      * You need to create you superuser account :

        $ python manage.py createsuperuser

You're ready now to run it :)

Run :

    $ python manage.py runserver 0.0.0.0:8000



Web services documentations :
    * You need to be logged-in :

        http://localhost:8000/login

    * Then checkout:

        http://localhost:8000/api/docs/

python manage.py collectstatic