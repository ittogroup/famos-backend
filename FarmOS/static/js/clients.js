
farmOsApp.controller('ClientsController', function ($scope, $timeout, Clients, Cities) {
    Clients.getClients().then(function(clients){
        $scope.clients = clients;
    });

     Cities.getCities().then(function(cities){
        $scope.cities = cities;
     });


    $scope.setDataToUpdate = function(client){
        $scope.currentSelected = client;
        $scope.clientToUpdate = angular.copy(client);
        $scope.cityToUpdate = angular.copy(client.city);
    }

    $scope.addClient = function(){
        if($scope.cityId == undefined || $scope.clientName == undefined || $scope.clientAddress == undefined || $scope.clientPhone == undefined ||
           $scope.cityId.length == 0 || $scope.clientName.length == 0 || $scope.clientAddress.length == 0 || $scope.clientPhone.length == 0){
            $scope.show = true;
            return false;
        }else{
            $scope.show = false;
        }
        var clientData = {
            "name": $scope.clientName,
            "address": $scope.clientAddress,
            "phone": $scope.clientPhone,
            "city_id": $scope.cityId,
        };

         Clients.addClient(clientData).then(function(data){
            $scope.clients.push(data);
            $scope.clientName = "";
            $scope.clientAddress = "";
            $scope.clientPhone = "";
            $scope.cityId = undefined;
         });
    }

      $scope.updateClient = function(){
        if($scope.clientToUpdate.city.id == undefined || $scope.clientToUpdate.name == undefined || $scope.clientToUpdate.address == undefined ||$scope.clientToUpdate.phone == undefined ||
           $scope.clientToUpdate.city.id.length == 0 || $scope.clientToUpdate.name.length == 0 || $scope.clientToUpdate.address.length == 0|| $scope.clientToUpdate.phone.length == 0){
            $scope.show = true;
            return false;
        }else{
            $scope.show = false;
        }
        var clientData = {
            "name": $scope.clientToUpdate.name,
            "address" : $scope.clientToUpdate.address,
            "phone" : $scope.clientToUpdate.phone,
            "city_id": $scope.cityToUpdate.id,
        };

         Clients.updateClient($scope.clientToUpdate.id, clientData).then(function(data){
            $scope.currentSelected.name = $scope.clientToUpdate.name;
            $scope.currentSelected.address = $scope.clientToUpdate.address;
            $scope.currentSelected.phone = $scope.clientToUpdate.phone;
            $scope.currentSelected.city =  $scope.cityToUpdate;
            $scope.clientToUpdate.name = "";
            $scope.clientToUpdate.address = "";
            $scope.clientToUpdate.phone = "";
            $scope.clientToUpdate.city = undefined;
             $("#updateClientDialog").modal("toggle");
         });
    }


    $scope.deleteClient = function(client){
        Clients.deleteClient(client.id).then(function(result){
            console.log(result);
             if (result.status == 204){
                var index = $scope.clients.indexOf(client);
                $scope.clients.splice(index, 1);
              }
        });
    }

});


var clientsServers = angular.module('Clients', ['ngCookies']);

clientsServers.factory('Clients', function($http, $cookies) {
   return {
    getClients: function() {

             return $http.get('/api/clients/')
                       .then(function(result) {
                            return result.data;
                        });
        },
        addClient: function(clientData) {

             return $http({
             method: "POST",
             url: '/api/clients/',
             data: clientData,
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid'),
                    'Content-Type': 'application/json'
                }})
             .then(function(result) {
                 return result.data;
                 });
        },
         deleteClient: function(client_id) {

             return $http({
             method: "DELETE",
             url: '/api/clients/' + client_id + "/",
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid')
                      }
                      })
                .then(function(result) {
                    return result;
                });
        },
         updateClient: function(client_id, clientData) {

             return $http({
             method: "PUT",
             url: '/api/clients/' + client_id + "/",
             data: clientData,
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid')
                      }
                      })
                .then(function(result) {
                    return result;
                });
        }

}});

farmOsApp.requires.push('Clients');

