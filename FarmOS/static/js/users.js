farmOsApp.controller('UserController', function ($scope, $timeout, User) {
     User.getUsers().then(function(users){
        $scope.users = users;
    });

    $scope.deleteOnClick = function(user){
       User.deleteUser(user.id).then(function(result){
            console.log(result);
             if (result.status == 204){
                var index = $scope.users.indexOf(user);
                $scope.users.splice(index, 1);
              }
        });
    }
    $scope.isStaff = false;
    $scope.addUser = function(){
        var userData = {
            "username": $scope.username,
            "first_name": $scope.firstName,
            "last_name": $scope.lastName,
            "password": $scope.password,

        };

         User.addUser(userData).then(function(data){
            $scope.users.push(data);
            $("#addUserDialog").modal("toggle");
            $scope.username = "";
            $scope.firstName = "";
            $scope.lastName = "";
            $scope.password = "";
         });
    }
});


var userApp = angular.module('User', ['ngCookies']);

userApp.factory('User', function($http, $cookies) {
   return {

         getUsers: function() {

             return $http.get('/api/users/')
                       .then(function(result) {
                            return result.data;
                        });
        },

        addUser: function(userData) {

             return $http({
             method: "POST",
             url: '/api/users/',
             data: userData,
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid'),
                    'Content-Type': 'application/json'
                }})
             .then(function(result) {
                 return result.data;
                 });
        },

        deleteUser: function(userId) {

             return $http({
             method: "DELETE",
             url: '/api/users/' + userId + "/",
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid')
                      }
                      })
                .then(function(result) {
                    return result;
                });
        }
   }
});

farmOsApp.requires.push('User');