farmOsApp.controller('BlController', function ($scope, $timeout, Bl, Cities) {
   $scope.maxBlsInPage = 20;
   $scope.currentOffset = 0;
   $scope.isHidePagination = true;
    $scope.getBls = function(offset){
        $scope.bls = [];
        $scope.isHidePagination = true;
        Bl.getBls($scope.maxBlsInPage, offset).then(function(bls){

            //Pagination
            $scope.bls = bls["results"];
            var nbrPage = bls["count"]/$scope.maxBlsInPage;
            if(nbrPage - parseInt( nbrPage, 10) != 0){
               nbrPage = parseInt( nbrPage, 10) + 1;
             }
            if(nbrPage > 1){
                $scope.isHidePagination = false;
            }else{
                $scope.isHidePagination = true;
            }
            $scope.nbrBls = nbrPage;
            $scope.currentOffset = offset;

            });
    }
    $scope.getBls(0);
    $scope.getBlPallets = function(bl, fetchData) {
      if(fetchData){
            Bl.getBlPallets(bl.id).then(function(pallets){
                 bl.pallets = pallets;
            });
       }
    }

});


var blServers = angular.module('Bl', ['ngCookies']);

blServers.factory('Bl', function($http, $cookies) {
   return {
    getBls: function(maxPalletInPage, offset) {

             return $http.get('/api/bl/?limit=' + maxPalletInPage + '&offset='+offset)
                       .then(function(result) {
                            return result.data;
                        });
        },
         getBlPallets: function(blId) {

             return $http.get('/api/bl/PalletByBL/?bl_id='+blId)
                       .then(function(result) {
                            return result.data;
                        });
        }

}});

farmOsApp.requires.push('Bl');