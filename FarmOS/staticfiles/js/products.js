farmOsApp.controller('ProductsController', function($scope, $timeout, Products) {
  Products.getProducts().then(function(products) {
    $scope.products = products;
  });

  $scope.addProduct = function() {
    if ($scope.productName == undefined ||
      $scope.productName.length == 0 ||
      $scope.productCode == undefined ||
      $scope.productCode.length == 0) {
      $scope.show = true;
      return false;
    } else {
      $scope.show = false;
    }
    var productData = {
      "name": $scope.productName,
      "code": $scope.productCode,
    };

    Products.addProduct(productData).then(function(data) {
      $scope.products.push(data);
      $scope.productName = "";
      $scope.productCode = "";
    });
  }

  $scope.deleteProduct = function(product) {
    Products.deleteProduct(product.id).then(function(result) {
      if (result.status == 204) {
        var index = $scope.products.indexOf(product);
        $scope.products.splice(index, 1);
      }
    });
  }

});


var productsServices = angular.module('Products', ['ngCookies']);

productsServices.factory('Products', function($http, $cookies) {
  return {
    getProducts: function() {

      return $http.get('/api/products/')
        .then(function(result) {
          return result.data;
        });
    },
    addProduct: function(productData) {

      return $http({
          method: "POST",
          url: '/api/products/',
          data: productData,
          headers: {
            'X-CSRFToken': $cookies.get('csrftoken'),
            'sessionid': $cookies.get('sessionid'),
            'Content-Type': 'application/json'
          }
        })
        .then(function(result) {
          return result.data;
        });
    },
    deleteProduct: function(productId) {

      return $http({
          method: "DELETE",
          url: '/api/products/' + productId + "/",
          headers: {
            'X-CSRFToken': $cookies.get('csrftoken'),
            'sessionid': $cookies.get('sessionid')
          }
        })
        .then(function(result) {
          return result;
        });
    }

  }
});

farmOsApp.requires.push('Products');
