farmOsApp.controller('StocksController', function($scope, $timeout, Stocks, Clients, RealTime) {
   $scope.maxPalletInPage = 20;
   $scope.currentOffset = 0;
   $scope.isHidePagination = true;
  RealTime.newStockCallNotification(function(data) {

    $scope.pallets.push(data);

    $scope.$apply();
  });

  Clients.getClients().then(function(clients) {
    $scope.clients = clients;
  });

  $scope.getStocks = function(offset){
    $scope.pallets = [];
    $scope.isHidePagination = true;
    Stocks.getStocks($scope.maxPalletInPage, offset).then(function(pallets) {

        $scope.pallets = pallets["results"];
        var nbrPage = pallets["count"]/$scope.maxPalletInPage;
        if(nbrPage - parseInt( nbrPage, 10) != 0){
           nbrPage = parseInt( nbrPage, 10) + 1;
         }
        if(nbrPage > 1){
            $scope.isHidePagination = false;
        }else{
            $scope.isHidePagination = true;
        }
        $scope.nbrPallets = nbrPage;
        $scope.currentOffset = offset;
      });
  }
   $scope.getStocks(0);
  $scope.updatepalletclient = function(pallet, clientid){
    var parceldata = {
            "client_id": parseint(clientid),
            "pallet_id": pallet.id,
        };
    stocks.updatepalletclient(parceldata).then(function(palletclient) {
        pallet.destination = palletclient["destination"];
    });

  }
});

var palletsServices = angular.module('Stocks', ['ngCookies']);

palletsServices.factory('Stocks', function($http, $cookies) {

  return {
    getStocks: function(maxPalletInPage, offset) {

      return $http.get('/api/pallets/?limit=' + maxPalletInPage + '&offset='+offset)
        .then(function(result) {
          return result.data;
        });
    },

    updatePalletClient: function(palletClientData) {

      return $http({
          method: "PUT",
          url: '/api/pallets/update_client/',
          data: palletClientData,
          headers: {
            'X-CSRFToken': $cookies.get('csrftoken'),
            'sessionid': $cookies.get('sessionid'),
            'Content-Type': 'application/json'
          }
        })
        .then(function(result) {
          return result.data;
        });
    }

  }
});

farmOsApp.requires.push('Stocks');
