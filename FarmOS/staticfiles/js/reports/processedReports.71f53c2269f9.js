farmOsApp.controller('ProcessedReportsController', function ($scope, $timeout, $filter, ProcessedDailyReport) {
  $scope.dateOptions = {
    // dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2030, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };
  
  $scope.select = function (dateValue) {
    var selectedDate = $filter('date')(new Date(dateValue), 'dd/MM/yyyy');
    var data = ProcessedDailyReport.getProcessedDailyReport(selectedDate).then(function (data) {
      $scope.processedDaily = {};
      data.forEach(element => {
              var keyId = element.stock.variety.name + " " + element.stock.variety.parcel.name

        if ($scope.processedDaily[keyId] == undefined) {
          $scope.processedDaily[keyId] = {
            "codeVariety": element.stock.variety.id,
            "variety": element.stock.variety.name,
            "parcel": element.stock.variety.parcel.name,
            "nbPallets": 1,
            "nbUnits": element.stock.units,
            "weight": parseFloat(element.stock.weight),
          };
        } else {
          $scope.processedDaily[keyId].nbPallets += 1;
          $scope.processedDaily[keyId].nbUnits += element.stock.units;
          $scope.processedDaily[keyId].weight += parseFloat(element.stock.weight);
        }

      });
      $scope.stockDailyToShow = Object.keys($scope.processedDaily).map(function (key) {
        return $scope.processedDaily[key];
      });
    });;
  };
  $scope.today = function () {
    $scope.dt = new Date();
    $scope.select($scope.dt);
  };
  $scope.today();

  $scope.clear = function () {
    $scope.dt = null;
  };
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }
  $scope.toggleMin = function () {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }
  $scope.format = 'dd-MMMM-yyyy';
  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };
  $scope.popup = {
    opened: false
  };
  $scope.toggleMin();

  $scope.open = function () {
    $scope.popup.opened = true;
  };
  
});

var processedDailyReportServices = angular.module('ProcessedDailyReport', ['ngCookies']);

processedDailyReportServices.factory('ProcessedDailyReport', function ($http, $cookies) {
  return {
    getProcessedDailyReport: function (date) {

      return $http.get('/api/daily_reports/get_daily_processed?date=' + date)
        .then(function (result) {
          return result.data;
        });
    }

  }
});

farmOsApp.requires.push('ProcessedDailyReport');
