farmOsApp.controller('StockByVarietyReportsController', function ($scope, $timeout, $filter, $routeParams, Varieties, StockByVarietyDailyReport) {
     Varieties.getVarieties().then(function(varieties){
        varietiesObj = {};
        $scope.format = 'dd-MMMM-yyyy HH:MM:ss';

        $scope.bigDate = '2218-02-26T11:45:10.244396Z';
        for (var i = 0; i < varieties.length; i++){
            varietiesObj[varieties[i].id] = varieties[i];
            varietiesObj[varieties[i].id].s_weight = 0;
            varietiesObj[varieties[i].id].p_weight = 0;
            varietiesObj[varieties[i].id].t_weight = 0;
            varietiesObj[varieties[i].id].t_pallet = 0;
            varietiesObj[varieties[i].id].minDate = $scope.bigDate;
        }

        StockByVarietyDailyReport.getStockByVarietyReport().then(function(dataWeight){
             //console.log(dataWeight);
             var stock = dataWeight.Stock;
             for (var i = 0; i < stock.length; i++) {
                 varietiesObj[stock[i].id].s_weight = stock[i].sum;
             }
             for (var i = 0; i < varieties.length; i++){
                if( varietiesObj[varieties[i].id].s_weight == 0){
                    delete varietiesObj[varieties[i].id];
               }
            }
             var processedStock = dataWeight.ProcessedStock;
             for (var i = 0; i < processedStock.length; i++) {
                 varietiesObj[processedStock[i].id].p_weight = processedStock[i].sum;
             }
             var minDate = dataWeight.MinDate;
             for (var i = 0; i < minDate.length; i++) {
                 varietiesObj[minDate[i].id].minDate = new Date(minDate[i].date);
                 varietiesObj[minDate[i].id].t_pallet = minDate[i].pallet_count;

             }

             $scope.varieties = [];
             for(var key in varietiesObj) {
                $scope.varieties.push(varietiesObj[key]);
             }
        });
    });


});

var stockByVarietyReportServices = angular.module('StockByVarietyDailyReport', ['ngCookies']);

stockByVarietyReportServices.factory('StockByVarietyDailyReport', function ($http, $cookies) {
  return {
    getStockByVarietyReport: function () {

      return $http.get('/api/general_reports/get_stock_by_variety')
        .then(function (result) {
          return result.data;
        });
    }

  }
});

farmOsApp.requires.push('StockByVarietyDailyReport');
