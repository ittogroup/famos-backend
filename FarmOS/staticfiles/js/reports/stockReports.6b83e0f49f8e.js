farmOsApp.controller('StockReportsController', function ($scope, $timeout, $filter, $routeParams, StockDailyReport) {
  $scope.dateOptions = {
    // dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2030, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };
       $scope.params = $routeParams;


  $scope.select = function (dateValue) {
    var selectedDate = $filter('date')(new Date(dateValue), 'dd/MM/yyyy');
    var data = StockDailyReport.getStockDailyReport(selectedDate).then(function (data) {
      $scope.stockDaily = {};
      data.forEach(element => {
        var keyId = element.variety.name + " " + element.variety.parcel.name
        if ($scope.stockDaily[keyId] == undefined) {
          $scope.stockDaily[keyId] = {
            "codeVariety": element.variety.id,
            "variety": element.variety.name,
            "parcel": element.variety.parcel.name,
            "nbPallets": 1,
            "nbUnits": element.units,
            "weight": parseFloat(element.weight),
          };

        } else {
          $scope.stockDaily[keyId].nbPallets += 1;
          $scope.stockDaily[keyId].nbUnits += element.units;
          $scope.stockDaily[keyId].weight += parseFloat(element.weight);
        }

      });



      $scope.stockDailyToShow = Object.keys($scope.stockDaily).map(function (key) {
        return $scope.stockDaily[key];
      });
    });;
  };
  $scope.today = function () {
  if($scope.params.date == undefined){
      $scope.dt = new Date();

  }else{
   $scope.dt = new Date($scope.params.date);
  }
    $scope.select($scope.dt);
  };
  $scope.today();

  $scope.clear = function () {
    $scope.dt = null;
  };
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }
  $scope.toggleMin = function () {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }
  $scope.format = 'dd-MMMM-yyyy';
  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };
  $scope.popup = {
    opened: false
  };
  $scope.toggleMin();

  $scope.open = function () {
    $scope.popup.opened = true;
  };
  
});

var stockDailyReportServices = angular.module('StockDailyReport', ['ngCookies']);

stockDailyReportServices.factory('StockDailyReport', function ($http, $cookies) {
  return {
    getStockDailyReport: function (date) {

      return $http.get('/api/daily_reports/get_daily_stock?date=' + date)
        .then(function (result) {
          return result.data;
        });
    }

  }
});

farmOsApp.requires.push('StockDailyReport');
