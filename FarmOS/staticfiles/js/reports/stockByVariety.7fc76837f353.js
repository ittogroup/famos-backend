farmOsApp.controller('StockByVarietyReportsController', function ($scope, $timeout, $filter, $routeParams, Varieties, StockByVarietyDailyReport) {
     Varieties.getVarieties().then(function(varieties){
        $scope.varieties = {};
        for (var i = 0; i < varieties.length; i++){
            $scope.varieties[varieties[i].id] = varieties[i];
            $scope.varieties[varieties[i].id].s_weight = 0;
            $scope.varieties[varieties[i].id].p_weight = 0;
            $scope.varieties[varieties[i].id].t_weight = 0;
        }

        StockByVarietyDailyReport.getStockByVarietyReport().then(function(dataWeight){
             var stock = dataWeight.Stock;
             for (var i = 0; i < stock.length; i++) {
                 $scope.varieties[stock[i].id].s_weight = stock[i].sum;
             }
             for (var i = 0; i < varieties.length; i++){
           if( $scope.varieties[varieties[i].id].s_weight == 0){
           delete $scope.varieties[varieties[i].id];
           }
        }
             var processedStock = dataWeight.ProcessedStock;
             for (var i = 0; i < processedStock.length; i++) {
                 $scope.varieties[processedStock[i].id].p_weight = processedStock[i].sum;
             }
        });
    });


});

var stockByVarietyReportServices = angular.module('StockByVarietyDailyReport', ['ngCookies']);

stockByVarietyReportServices.factory('StockByVarietyDailyReport', function ($http, $cookies) {
  return {
    getStockByVarietyReport: function () {

      return $http.get('/api/general_reports/get_stock_by_variety')
        .then(function (result) {
          return result.data;
        });
    }

  }
});

farmOsApp.requires.push('StockByVarietyDailyReport');
