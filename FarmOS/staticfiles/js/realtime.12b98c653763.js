(function() {

  var realTimeService = angular.module('RealTime', []);

  realTimeService.factory('RealTime', function($websocket) {
    var loc = window.location,
      new_uri;
    if (loc.protocol === "https:") {
      new_uri = "wss:";
    } else {
      new_uri = "ws:";
    }
    new_uri += "//" + loc.host;
    console.log(new_uri);
    var ws = $websocket.$new({
      url: new_uri,
      protocols : [],
      subprotocols : ['base46'],
      reconnect: true // it will reconnect after 2 seconds
    });
    var newStockCallbackNotification = undefined;
    var callbackNotification = undefined;

    ws.$on('$open', function() {
        console.log('Connection opened');
        // ws.$emit('newStock', 'hi listening websocket server');
      })

      .$on('newStock', function(data) {
        if (newStockCallbackNotification !== undefined) {
          newStockCallbackNotification(data);
        }
        if (callbackNotification !== undefined) {
          callbackNotification('newStock', data);
        }
      })
      .$on('$close', function() {
        console.log('Connection closed');
      });
    return {
      newStockCallNotification: function(callback) {
        newStockCallbackNotification = callback;
      },
      notification: function(callback) {
        callbackNotification = callback;
      },
      sendData: function() {
        ws.$emit('newStock', 'new');
      }
    }
  });

  farmOsApp.requires.push('RealTime');
}());
