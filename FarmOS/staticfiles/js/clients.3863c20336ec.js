farmOsApp.controller('ClientsController', function ($scope, $timeout, Clients, Cities) {
    Clients.getClients().then(function(clients){
        $scope.clients = clients;
    });

     Cities.getCities().then(function(cities){
        $scope.cities = cities;
     });

    $scope.addClient = function(){
        if($scope.cityId == undefined || $scope.clientName == undefined ||
           $scope.cityId.length == 0 || $scope.clientName.length == 0){
            $scope.show = true;
            return false;
        }else{
            $scope.show = false;
        }
        var clientData = {
            "name": $scope.clientName,
            "city_id": $scope.cityId,
        };

         Clients.addClient(clientData).then(function(data){
            $scope.clients.push(data);
            $scope.clientName = "";
         });
    }

    $scope.deleteClient = function(client){
        Clients.deleteClient(client.id).then(function(result){
            console.log(result);
             if (result.status == 204){
                var index = $scope.clients.indexOf(client);
                $scope.clients.splice(index, 1);
              }
        });
    }

});


var clientsServers = angular.module('Clients', ['ngCookies']);

clientsServers.factory('Clients', function($http, $cookies) {
   return {
    getClients: function() {

             return $http.get('/api/clients/')
                       .then(function(result) {
                            return result.data;
                        });
        },
        addClient: function(clientData) {

             return $http({
             method: "POST",
             url: '/api/clients/',
             data: clientData,
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid'),
                    'Content-Type': 'application/json'
                }})
             .then(function(result) {
                 return result.data;
                 });
        },
         deleteClient: function(client_id) {

             return $http({
             method: "DELETE",
             url: '/api/clients/' + client_id + "/",
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid')
                      }
                      })
                .then(function(result) {
                    return result;
                });
        }

}});

farmOsApp.requires.push('Clients');