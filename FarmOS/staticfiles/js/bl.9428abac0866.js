farmOsApp.controller('BlController', function ($scope, $timeout, Bl, Cities) {

    Bl.getBls().then(function(bls){
        $scope.bls = bls;
    });


});


var blServers = angular.module('Bl', ['ngCookies']);

blServers.factory('Bl', function($http, $cookies) {
   return {
    getBls: function() {

             return $http.get('/api/bl/')
                       .then(function(result) {
                            return result.data;
                        });
        }

}});

farmOsApp.requires.push('Bl');