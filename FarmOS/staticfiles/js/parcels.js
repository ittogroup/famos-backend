farmOsApp.controller('ParcelsController', function ($scope, $timeout, Products, Parcels) {
    Products.getProducts().then(function(products){
        $scope.products = products;
    });

     Parcels.getParcels().then(function(parcels){
        $scope.parcels = parcels;
    });


    $scope.addParcel = function(){
        var parcelData = {
            "name": $scope.parcelName,
            "product_id": $scope.productId,
        };

         Parcels.addParcel(parcelData).then(function(data){
            $scope.parcels.push(data);
            $scope.parcelName = "";
         });
    }

    $scope.deleteParcel = function(parcel){
        Parcels.deleteParcel(parcel.id).then(function(result){
            console.log(result);
             if (result.status == 204){
                var index = $scope.parcels.indexOf(parcel);
                $scope.parcels.splice(index, 1);
              }
        });
    }

});


var parcelsServices = angular.module('Parcels', ['ngCookies']);

parcelsServices.factory('Parcels', function($http, $cookies) {
   return {
    getParcels: function() {

             return $http.get('/api/parcels/')
                       .then(function(result) {
                            return result.data;
                        });
        },
        addParcel: function(parcelData) {

             return $http({
             method: "POST",
             url: '/api/parcels/',
             data: parcelData,
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid'),
                    'Content-Type': 'application/json'
                }})
             .then(function(result) {
                 return result.data;
                 });
        },
         deleteParcel: function(parcelId) {

             return $http({
             method: "DELETE",
             url: '/api/parcels/' + parcelId + "/",
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid')
                      }
                      })
                .then(function(result) {
                    return result;
                });
        }

}});

farmOsApp.requires.push('Parcels');