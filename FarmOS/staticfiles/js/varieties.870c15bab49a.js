farmOsApp.controller('VarietiesController', function ($scope, $timeout, Varieties, Parcels, Products) {

    $scope.productId = undefined;
    $scope.parcelId = undefined;

    Varieties.getVarieties().then(function(varieties){
        $scope.varieties = varieties;
    });

    Products.getProducts().then(function(products) {
        $scope.products = products;
    });

     Parcels.getParcels().then(function(parcels){
        $scope.parcels = parcels;
     });

    $scope.addVariety = function(){
        if($scope.parcelId == undefined || $scope.varietyName == undefined ||
           $scope.parcelId.length == 0 || $scope.varietyName.length == 0){
            $scope.show = true;
            return false;
        }else{
            $scope.show = false;
        }
        var varietyData = {
            "name": $scope.varietyName,
            "parcel_id": $scope.parcelId,
        };

         Varieties.addVariety(varietyData).then(function(data){
            $scope.varieties.push(data);
            $scope.varietyName = "";
         });
    }

    $scope.deleteVariety = function(variety){
        Varieties.deleteVariety(variety.id).then(function(result){
            console.log(result);
             if (result.status == 204){
                var index = $scope.varieties.indexOf(variety);
                $scope.varieties.splice(index, 1);
              }
        });
    }

});


var varietiesServers = angular.module('Varieties', ['ngCookies']);

varietiesServers.factory('Varieties', function($http, $cookies) {
   return {
    getVarieties: function() {

             return $http.get('/api/varieties/')
                       .then(function(result) {
                            return result.data;
                        });
        },
        addVariety: function(varietyData) {

             return $http({
             method: "POST",
             url: '/api/varieties/',
             data: varietyData,
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid'),
                    'Content-Type': 'application/json'
                }})
             .then(function(result) {
                 return result.data;
                 });
        },
         deleteVariety: function(variety_id) {

             return $http({
             method: "DELETE",
             url: '/api/varieties/' + variety_id + "/",
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid')
                      }
                      })
                .then(function(result) {
                    return result;
                });
        }

}});

farmOsApp.requires.push('Varieties');