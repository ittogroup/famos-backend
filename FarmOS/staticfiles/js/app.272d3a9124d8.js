var farmOsApp = angular.module('farmos.app', ['ui.bootstrap', 'angular-loading-bar', 'angular-click-outside', 'ngRoute',
  'ngWebsocket', 'ngToast'
]);


farmOsApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = false;
}]);

farmOsApp.config(function($routeProvider) {
  $routeProvider
    .when("/users", {
      templateUrl: "/users",
      controller: 'UserController'
    })
    .when("/varieties", {
      templateUrl: "/varieties",
      controller: 'VarietiesController'
    })
    .when("/parcels", {
      templateUrl: "parcels",
      controller: 'ParcelsController'

    })
    .when("/products", {
      templateUrl: "/products",
      controller: 'ProductsController'
    })
    .when("/stocks", {
      templateUrl: "/stocks",
      controller: 'StocksController'
    })
    .when("/clients", {
      templateUrl: "/clients",
      controller: 'ClientsController'
    })
    .when("/stocksReports", {
      templateUrl: "/stocksReports",
      controller: 'StockReportsController'
    })
    .when("/stockPrintReport/:date", {
      templateUrl: "/stockPrintReport",
      controller: 'StockReportsController'
    })
    .when("/processedReports", {
      templateUrl: "/processedReports",
      controller: 'ProcessedReportsController'
    })
    .when("/palletReports", {
      templateUrl: "/palletReports",
      controller: 'ClientsController'
    })
    .when("/StockByVarietyReports", {
      templateUrl: "/StockByVarietyReports",
      controller: 'StockByVarietyReportsController'
    });

});
