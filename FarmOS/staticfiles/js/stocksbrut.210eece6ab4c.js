farmOsApp.controller('StocksBrutController', function($scope, $timeout, StocksBrut, Clients, RealTime) {

   StocksBrut.getStocksBrut().then(function(stocksBrut) {
    $scope.stocksBrut = stocksBrut;
  });
   $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    $scope.archive = function(stock){
        var archived = 't';
        if(!stock.archived){
            archived = 'f';
            stock.archived = true;
        }else{
            stock.archived = false;
        }

        StocksBrut.archiveStock(stock.id, archived).then(function(stocksBrut) {
            stock.archived = stocksBrut[0].archived;
         });

    }
});

var palletsServices = angular.module('StocksBrut', ['ngCookies']);

palletsServices.factory('StocksBrut', function($http, $cookies) {
  return {
    getStocksBrut: function() {

      return $http.get('/api/stocksBrut/get_stocks_brut')
        .then(function(result) {
          return result.data;
        });
    },
    archiveStock: function(id, status) {

      return $http.get('/api/stocksBrut/get_archive_stock/?id=' + id +'&archived=' + status)
        .then(function(result) {
          return result.data;
        });
    }

  }
});

farmOsApp.requires.push('StocksBrut');
