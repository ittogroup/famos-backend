var citiesServices = angular.module('Cities', ['ngCookies']);

citiesServices.factory('Cities', function($http, $cookies) {
  return {
    getCities: function() {

      return $http.get('/api/cities/')
        .then(function(result) {
          return result.data;
        });
    }

  }
});

farmOsApp.requires.push('Cities');
