farmOsApp.controller('StocksController', function($scope, $timeout, Stocks, Clients, RealTime) {
  RealTime.newStockCallNotification(function(data) {

    $scope.pallets.push(data);

    $scope.$apply();
  });

  Clients.getClients().then(function(clients) {
    $scope.clients = clients;
  });

  Stocks.getStocks().then(function(pallets) {
    $scope.pallets = pallets;
  });

  $scope.updatePalletClient = function(pallet, clientId){
    var parcelData = {
            "client_id": parseInt(clientId),
            "pallet_id": pallet.id,
        };
    Stocks.updatePalletClient(parcelData).then(function(palletClient) {
        pallet.destination = palletClient["destination"];
    });

  }
});

var palletsServices = angular.module('Stocks', ['ngCookies']);

palletsServices.factory('Stocks', function($http, $cookies) {
  return {
    getStocks: function() {

      return $http.get('/api/pallets/')
        .then(function(result) {
          return result.data;
        });
    },

    updatePalletClient: function(palletClientData) {

      return $http({
          method: "PUT",
          url: '/api/pallets/update_client/',
          data: palletClientData,
          headers: {
            'X-CSRFToken': $cookies.get('csrftoken'),
            'sessionid': $cookies.get('sessionid'),
            'Content-Type': 'application/json'
          }
        })
        .then(function(result) {
          return result.data;
        });
    }

  }
});

farmOsApp.requires.push('Stocks');
