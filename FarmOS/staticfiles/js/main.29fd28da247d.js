
farmOsApp.controller('MainController', function($scope, $timeout, RealTime, ngToast) {


  RealTime.notification(function(event, data){
    if(event == 'newStock'){
      ngToast.create('New Pallet ('+ data['id']+') is created :' +  data['product']['name']);
    }
    $scope.$apply();
  });

  $scope.select = function(item) {
    $scope.selected = item;
  };
  $scope.isActive = function(item) {
    return $scope.selected === item;
  };

  $scope.openNav = function() {
    document.getElementById("side-nav").style.width = "250px";
  }

  $scope.closeNav = function() {
    document.getElementById("side-nav").style.width = "0";
    return false;
  }

});
