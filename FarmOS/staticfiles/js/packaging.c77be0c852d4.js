farmOsApp.controller('PackagingController', function ($scope, $timeout, Packaging, Cities) {

    Packaging.getPackaging().then(function(packaging){
        $scope.packaging = packaging;
    });

    $scope.setDataToUpdate = function(packaging){
        $scope.currentSelected = packaging;
        $scope.packagingToUpdate = angular.copy(packaging);
    }


    $scope.addPackaging = function(){
        if($scope.packagingName == undefined || $scope.packagingName == undefined ||
           $scope.packagingWeight == 0 || $scope.packagingWeight == undefined ||
           $scope.packagingType == 0 || $scope.packagingType == undefined){
            $scope.show = true;
            return false;
        }else{
            $scope.show = false;
        }
        var packagingData = {
            "name": $scope.packagingName,
            "weight": $scope.packagingWeight,
            "packaging_type": $scope.packagingType,
        };

         Packaging.addPackaging(packagingData).then(function(data){
            $scope.packaging.push(data);
            $scope.packagingName = "";
            $scope.packagingWeight = "";
            $scope.packagingType = undefined;
         });
    }

    $scope.updatePackaging = function(){
        if($scope.packagingToUpdate.name == undefined || $scope.packagingToUpdate.name == undefined ||
           $scope.packagingToUpdate.weight == 0 || $scope.packagingToUpdate.weight == undefined ||
           $scope.packagingToUpdate.packaging_type == 0 || $scope.packagingToUpdate.packaging_type == undefined){
            $scope.showUpdate = true;
            return false;
        }else{
            $scope.showUpdate = false;
        }
        var packagingData = {
            "name": $scope.packagingToUpdate.name,
            "weight": $scope.packagingToUpdate.weight,
            "packaging_type": $scope.packagingToUpdate.packaging_type,
        };

         Packaging.updatePackaging($scope.packagingToUpdate.id ,packagingData).then(function(data){
            $scope.currentSelected.name = $scope.packagingToUpdate.name;
            $scope.currentSelected.weight = $scope.packagingToUpdate.weight;
             $scope.currentSelected.packaging_type =  $scope.packagingToUpdate.packaging_type;
            $scope.packagingToUpdate.name = "";
            $scope.packagingToUpdate.weight = "";
            $scope.packagingToUpdate.packaging_type = undefined;
             $("#updatePackagingDialog").modal("toggle");
         });
    }

    $scope.deletePackaging = function(pack){
        Packaging.deletePackaging(pack.id).then(function(result){
            console.log(result);
             if (result.status == 204){
                var index = $scope.packaging.indexOf(pack);
                $scope.packaging.splice(index, 1);
              }
        });
    }
});


var packagingServers = angular.module('Packaging', ['ngCookies']);

packagingServers.factory('Packaging', function($http, $cookies) {
   return {
    getPackaging: function() {

             return $http.get('/api/packaging/')
                       .then(function(result) {
                            return result.data;
                        });
        },
        addPackaging: function(packagingData) {

             return $http({
             method: "POST",
             url: '/api/packaging/',
             data: packagingData,
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid'),
                    'Content-Type': 'application/json'
                }})
             .then(function(result) {
                 return result.data;
                 });
        },
         deletePackaging: function(packaging_id) {

             return $http({
             method: "DELETE",
             url: '/api/packaging/' + packaging_id + "/",
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid')
                      }
                      })
                .then(function(result) {
                    return result;
                });
        },
         updatePackaging: function(packaging_id, packagingData) {

             return $http({
             method: "PUT",
             url: '/api/packaging/' + packaging_id + "/",
             data: packagingData,
             headers: {
                    'X-CSRFToken': $cookies.get('csrftoken'),
                    'sessionid': $cookies.get('sessionid'),
                    'Content-Type': 'application/json'
                      }
                      })
                .then(function(result) {
                    return result;
                });
        }

}});

farmOsApp.requires.push('Packaging');