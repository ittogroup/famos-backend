from channels import Group
from channels.staticfiles import StaticFilesConsumer


def ws_connect(message):
    message.reply_channel.send({"accept": True})
    Group("itto").add(message.reply_channel)


# def ws_receive(message):
#     print(message["text"])
#     Group('itto').send({"text" : "{\"event\":\"test\",\"data\":\"hi listening websocket server\"}"}
# )


def ws_disconnect(message):
    Group("itto").discard(message.reply_channel)


channel_routing = {
    'http.request': StaticFilesConsumer(),

    'websocket.connect': ws_connect,
    # 'websocket.receive': ws_receive,
    'websocket.disconnect': ws_disconnect,
}
