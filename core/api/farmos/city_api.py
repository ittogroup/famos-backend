from rest_framework import viewsets
from core.models import City
from core.serializers.city_serializer import CitySerializer


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
