from datetime import datetime

from django.db import connection
from rest_framework import viewsets, generics, status
from rest_framework.decorators import list_route

from core.models import Stock, ProcessedStock, Pallet, Variety
from core.serializers.pallet_serializer import PalletSerializer
from core.serializers.processed_stock_serializer import ProcessedStockSerializer
from core.serializers.stock_seriliazer import StockSerializer
from rest_framework.response import Response


class GeneralReportsViewSet(viewsets.ViewSet):

    def dictfetchall(self, cursor):
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    @list_route(methods=['get'])
    def get_stock_by_variety(self, request):
        with connection.cursor() as cursor:
            cursor.execute('SELECT v.id , v.name, sum(s.weight - s.units* pa.weight -30)  '
                           'FROM core_stock s, core_variety v, core_packaging pa '
                           'WHERE s.variety_id = v.id AND s.packaging_id = pa.id GROUP BY v.id;')
            stock = self.dictfetchall(cursor)

            cursor.execute('SELECT v.id, v.name, sum(s.weight- s.units* pa.weight -30) '
                           'FROM core_processedstock p, core_stock s, core_variety v, core_packaging pa '
                           'WHERE p.stock_id = s.id AND v.id = s.variety_id AND s.packaging_id = pa.id GROUP BY v.id;')
            processed_stock = self.dictfetchall(cursor)

            cursor.execute('SELECT v.id, MIN(s.date) date, count(s.*) pallet_count FROM core_stock s, core_variety v '
                           'WHERE s.variety_id = v.id and s.processed = false GROUP BY v.id;')
            min_date = self.dictfetchall(cursor)

            all_data = {
                "Stock": stock,
                "ProcessedStock": processed_stock,
                "MinDate": min_date
            }
            return Response(all_data)


