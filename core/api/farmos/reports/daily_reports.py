from datetime import datetime

from rest_framework import viewsets, generics, status
from rest_framework.decorators import list_route

from core.models import Stock, ProcessedStock, Pallet
from core.serializers.pallet_serializer import PalletSerializer
from core.serializers.processed_stock_serializer import ProcessedStockSerializer
from core.serializers.stock_seriliazer import StockSerializer
from rest_framework.response import Response


class DailyReportsViewSet(viewsets.ViewSet):

    @list_route(methods=['get'])
    def get_daily_stock(self, request):
        date = self.request.query_params.get('date', None)
        dt = datetime.strptime(date, "%d/%m/%Y").date()
        queryset = Stock.objects.filter(date__date=dt)

        serializer = StockSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['get'])
    def get_daily_processed(self, request):
        date = self.request.query_params.get('date', None)
        dt = datetime.strptime(date, "%d/%m/%Y").date()
        queryset = ProcessedStock.objects.filter(date__date=dt)

        serializer = ProcessedStockSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['get'])
    def get_daily_pallets(self, request):
        date = self.request.query_params.get('date', None)
        dt = datetime.strptime(date, "%d/%m/%Y").date()
        queryset = Pallet.objects.filter(date__date=dt)

        serializer = PalletSerializer(queryset, many=True)
        return Response(serializer.data)
