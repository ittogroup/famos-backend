import coreapi
from django.http import Http404
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import list_route, api_view
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import Stock
from core.serializers.archive_item_serializer import ArchiveItemSerializer
from core.serializers.stock_seriliazer import StockSerializer
from core.serializers.stockbrut_seriliazer import StockBrutSerializer


class StocksBrutViewSet(viewsets.ViewSet):

    @list_route(methods=['get'])
    def get_stocks_brut(self, request):

        queryset = Stock.objects.filter(processed=False)

        serializer = StockBrutSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['get'])
    def get_archive_stock(self, request):
        pid = self.request.query_params.get('id', None)
        archived = self.request.query_params.get('archived', None)

        st = Stock.objects.get(id=pid)

        if 't' == archived:
            st.archived = True
        else:
            st.archived = False
        st.save()

        serializer = StockBrutSerializer([st], many=True)
        return Response(serializer.data)


class StockViewSet(viewsets.ModelViewSet):
    queryset = Stock.objects.filter(processed=False, archived=False)
    serializer_class = StockSerializer

    @list_route(methods=['get'])
    def searchstock(self, request, *args, **kwargs):
        data = Stock.objects.filter(processed=False)

        palletid = self.request.query_params.get('pallet_id', None)
        if palletid is None or palletid == u'':
            variety_id = self.request.query_params.get('variety_id', None)
            if variety_id is not None and variety_id != u'' and variety_id != u'0':
                data = data.filter(variety__id=variety_id)

            units = self.request.query_params.get('units', None)
            if units is not None and units != u'':
                data = data.filter(units=units)

            weight = self.request.query_params.get('weight', None)
            if weight is not None and weight != u'':
                data = data.filter(weight=weight)

            corporal = self.request.query_params.get('corporal', None)

            if corporal is not None and corporal != u'':
                data = data.filter(corporal=corporal)
        else:
            data = data.filter(id=palletid)
        queryset = self.filter_queryset(data).order_by('-date')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Stock.objects.filter(processed=False, id=pk)
        user = get_object_or_404(queryset)
        serializer = StockSerializer(user)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(Stock.objects.filter(processed=False, archived=False)).order_by('-date')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            serializer_context = {
                'request': Request(request),
            }

            stock_serializer = StockSerializer(instance=instance, context=serializer_context)

            self.perform_destroy(instance)
            return Response(stock_serializer.data, status=status.HTTP_200_OK)

        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class StockArchive(APIView):
    """
        Archive list of stock ids.

    """

    @staticmethod
    def get_serializer():
        return ArchiveItemSerializer()

    def put(self, request, format=None):

        item_serializer = ArchiveItemSerializer(data=request.data)
        item_serializer.is_valid(raise_exception=True)
        archive_ids = item_serializer.data['archive_ids']
        content = {'archive_ids': archive_ids,
                   'status': 'OK'}
        for id_to_archive in archive_ids:
            for e in Stock.objects.filter(id=id_to_archive):
                e.archived = True
                e.save()
        return Response(content, status=status.HTTP_200_OK)
