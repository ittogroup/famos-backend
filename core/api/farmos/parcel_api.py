from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from core.models import Parcel, Variety
from core.serializers.parcel_serializer import ParcelSerializer


class ParcelViewSet(viewsets.ModelViewSet):
    queryset = Parcel.objects.all()
    serializer_class = ParcelSerializer

    @list_route(url_path='byVariety/(?P<variety_id>[0-9]+)')
    def retrieve_by_variety_id(self, request, variety_id=None):
        try:
            variety = Variety.objects.get(id=variety_id)
        except Variety.DoesNotExist:
            return Response([])

        queryset = Parcel.objects.filter(variety=variety)
        serializer = ParcelSerializer(queryset,
                                      context={'request': request},
                                      many=True)
        return Response(serializer.data)
