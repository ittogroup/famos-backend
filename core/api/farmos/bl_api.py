from rest_framework import viewsets, status, generics
from rest_framework.decorators import list_route
from rest_framework.mixins import ListModelMixin, DestroyModelMixin
from rest_framework.views import APIView

from core.models import BL, Client, Pallet
from django.db.models import Q
from core.serializers.bl_serializer import BLSerializer
from rest_framework.response import Response

from core.serializers.pallet_serializer import PalletSerializer


class BLViewSet(viewsets.ViewSet,
                ListModelMixin,
                DestroyModelMixin,
                generics.GenericAPIView):
    queryset = BL.objects.filter(archived=False).order_by('-date')
    serializer_class = BLSerializer


class PalletByBL(APIView):

    def get(self, request):
        bl_id = request.GET["bl_id"]

        pallets = Pallet.objects.filter(bl_id=bl_id)
        sr = PalletSerializer(pallets, many=True).data
        return Response(sr, status=status.HTTP_200_OK)



class BLCreateUpdate(APIView):
    """
        Create update BL

    """

    def post(self, request):

        data = request.data
        client_id = data["client_id"]

        client = Client.objects.filter(id=client_id).first()
        bl = None
        if "bl_id" in data and data["bl_id"] is not None:
            bl_id = data["bl_id"]
            bl = BL.objects.filter(id=bl_id).first()
            bl.client = client

        if bl is None:
            bl = BL(client=client)

        pallets_ids = data["pallets_ids"]
        pallets_ids_remove = data["pallets_ids_remove"]

        for pallet_id in pallets_ids_remove:
            pallet = Pallet.objects.filter(Q(bl_id=bl.id, id=pallet_id)).first()
            if pallet is not None:
                pallet.bl = None
                pallet.save()

        # bl = BL(client=client)
        bl.save()
        data["pallets_ids"] = []
        for pallet_id in pallets_ids:
            pallet = Pallet.objects.filter(Q(id=pallet_id, bl=None) | Q(bl_id=bl.id, id=pallet_id)).first()
            if pallet is not None:
                pallet.bl = bl
                pallet.destination = client
                pallet.save()
                data["pallets_ids"].append(pallet.id)

        return Response(data, status=status.HTTP_200_OK)
