# -*- coding: utf-8 -*-
from datetime import datetime

from channels import Group
from django.http import Http404
from pyfcm import FCMNotification
from rest_framework import viewsets, status, generics
from rest_framework.decorators import list_route
from rest_framework.mixins import DestroyModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.request import Request
from rest_framework.response import Response
import json

from rest_framework.views import APIView

from core.models import Pallet, PalletEntry, Client, FcmToken, Packaging, Variety
from core.serializers.archive_item_serializer import ArchiveItemSerializer
from core.serializers.client_serializer import ClientSerializer
from core.serializers.pallet_entry_serializer import PalletEntrySerializer
from core.serializers.pallet_serializer import PalletSerializer
from core.serializers.update_pallet_client_serializer import UpdatePalletClientSerializer


class PalletViewSet(viewsets.ViewSet,
                    ListModelMixin,
                    RetrieveModelMixin,
                    DestroyModelMixin,
                    generics.GenericAPIView):
    queryset = Pallet.objects.filter(archived=False)
    serializer_class = PalletSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(Pallet.objects.filter(archived=False)).order_by('-date')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        try:
            instance = Pallet.objects.get(pk=pk)
            serializer_context = {
                'request': Request(request),
            }

            pallet_serializer = PalletSerializer(instance=instance, context=serializer_context)
            pallet_data = pallet_serializer.data
            self.perform_destroy(instance)
            return Response(pallet_data, status=status.HTTP_200_OK)

        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

    @list_route(url_path='save_pallet', methods=['post'])
    def save_pallet(self, request):
        serializer = PalletSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if request.data["entries"] is not None:
            pallet = serializer.save()
            pallet.processing_date = pallet.date
            pallet.save()
            for entry in request.data["entries"]:
                entry["pallet_id"] = pallet.id
                pallet_entry_serializer = PalletEntrySerializer(data=entry)
                pallet_entry_serializer.is_valid(raise_exception=True)
                pallet_entry_serializer.save()
        Group('itto').send({"text": "{\"event\":\"newStock\",\"data\":" + json.dumps(serializer.data) + "}"})

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @list_route(url_path='update_pallet', methods=['put'])
    def update_pallet(self, request):
        serializer = PalletSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        entries_to_remove = request.data["entries_to_remove"]
        if isinstance(entries_to_remove, list) and len(entries_to_remove) != 0:
            PalletEntry.objects.filter(pk__in=entries_to_remove).delete()
        rentries = request.data["entries"]

        pallet = Pallet.objects.get(pk=request.data["id"])
        tms = int(request.data["processing_date"] / 1000)
        dt_object = datetime.fromtimestamp(tms)
        pallet.processing_date = dt_object
        pallet_variety = Variety.objects.get(pk=request.data["variety_id"])
        pallet.variety = pallet_variety
        if rentries is not None:
            destination_id = None
            if "destination_id" in request.data.keys():
                destination_id = request.data["destination_id"]
            vrac = request.data["vrac"]
            pallet.vrac = vrac
            if destination_id is not None:
                pallet.destination = Client.objects.get(pk=destination_id)

            for entry in rentries:
                entry_id = entry["id"] if "id" in entry else None

                if entry_id is None:
                    entry["pallet_id"] = pallet.id
                    pallet_entry_serializer = PalletEntrySerializer(data=entry)
                    pallet_entry_serializer.is_valid(raise_exception=True)
                    pallet_entry_serializer.save()
                else:
                    pallet_entry = PalletEntry.objects.get(pk=entry_id)
                    pallet_entry.weight = entry["weight"]
                    pallet_entry.weight_gross = entry["weight_gross"]
                    pallet_entry.units = entry["units"]
                    pallet_entry.calibre = entry["calibre"]
                    packaging = Packaging.objects.filter(id=entry["packaging_id"]).first()
                    pallet_entry.packaging = packaging
                    pallet_entry.save()

        pallet.save()
        serializer = PalletSerializer(pallet)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class PalletArchive(APIView):
    """
        Archive list of Pallet ids.

    """

    @staticmethod
    def get_serializer():
        return ArchiveItemSerializer()

    def put(self, request, format=None):

        item_serializer = ArchiveItemSerializer(data=request.data)
        item_serializer.is_valid(raise_exception=True)
        archive_ids = item_serializer.data['archive_ids']
        content = {'archive_ids': archive_ids,
                   'status': 'OK'}
        for id_to_archive in archive_ids:
            for e in Pallet.objects.filter(id=id_to_archive):
                e.archived = True
                e.save()
        return Response(content, status=status.HTTP_200_OK)


class UpdatePalletClient(APIView):
    """
        Update pallet client destination

    """

    @staticmethod
    def get_serializer():
        return UpdatePalletClientSerializer()

    def put(self, request, format=None):
        pallet_client_serializer = UpdatePalletClientSerializer(data=request.data)
        pallet_client_serializer.is_valid(raise_exception=True)
        pallet_id = pallet_client_serializer.data['pallet_id']
        client_id = pallet_client_serializer.data['client_id']

        pallet = Pallet.objects.get(pk=pallet_id)
        client = None
        if client_id is not None:
            client = Client.objects.get(pk=client_id)
        pallet.destination = client
        pallet.save()
        serialized_client = None
        if client_id is not None:
            serialized_client = ClientSerializer(client).data
            try:
                fcm_token = FcmToken.objects.get(user_id="Expedition")

                registration_id = fcm_token.token
                message_title = "Update Pallet"
                message_body = "Send Pallet " + str(pallet.id) + " To " + client.name + "[ " + client.city.name + "]"
                push_service = FCMNotification(api_key="AAAAt_UH9sM:APA91bE9vfgbNOQJOgE2xTSrtoLreT2cYW0QkVPFibkY1Ad3_mSJQDMPTW3Ue7lCOBZGRlwolomxhgoOYbTyK8p4wStHccXkVowL3hxtMGkXtIaRQuCcw33zg84TgdUX-jyFuhhRlTQf")

                push_service.notify_single_device(registration_id=registration_id, message_title=message_title,
                                                       message_body=message_body)
            except FcmToken.DoesNotExist:
                print("fcm error")

        content = {
            'destination': serialized_client,
            'status': 'OK'}
        return Response(content, status=status.HTTP_200_OK)
