from rest_framework import viewsets

from core.models import Product
from core.serializers.product_serializer import ProductSerializer


class ProductsViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
