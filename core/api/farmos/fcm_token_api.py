import coreapi
from django.http import Http404
from rest_framework import status
from rest_framework import viewsets
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import FcmToken
from core.serializers.fmc_token_serializer import FcmTokenSerializer


class FcmTokenView(APIView):
    """
        Archive list of stock ids.

    """

    @staticmethod
    def get_serializer():
        return FcmTokenSerializer()

    def post(self, request, format=None):

        item_serializer = FcmTokenSerializer(data=request.data)
        item_serializer.is_valid(raise_exception=True)
        token = item_serializer.data['token']
        user_id = item_serializer.data['user_id']
        content = {'token': token, 'user_id': user_id, 'status': 'OK'}
        try:
            fcm_token = FcmToken.objects.get(user_id=user_id)
            fcm_token.token = token
        except FcmToken.DoesNotExist:
            fcm_token = FcmToken(user_id=user_id, token=token)

        fcm_token.save()
        return Response(content, status=status.HTTP_200_OK)
