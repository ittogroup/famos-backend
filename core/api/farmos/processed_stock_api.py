from rest_framework.generics import get_object_or_404
from rest_framework.views import APIView

from core.models import ProcessedStock, Stock
from core.serializers.archive_item_serializer import ArchiveItemSerializer
from core.serializers.processed_stock_serializer import ProcessedStockSerializer
from django.http import Http404
from rest_framework import status
from rest_framework import viewsets
from rest_framework.request import Request
from rest_framework.response import Response


class ProcessedStockViewSet(viewsets.ModelViewSet):
    queryset = ProcessedStock.objects.filter(archived=False)
    serializer_class = ProcessedStockSerializer

    def create(self, request, *args, **kwargs):

        stock_id = request.data['stock_id']
        stock_processed = ProcessedStock.objects.filter(stock__id=stock_id)
        a = stock_processed.count()
        if stock_processed.count() != 0:
            user = get_object_or_404(stock_processed)
            serializer = ProcessedStockSerializer(user)
            return Response(serializer.data)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        stock = Stock.objects.get(pk=stock_id)
        stock.processed = True
        stock.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(ProcessedStock.objects.filter(archived=False)).order_by('-date')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def destroy(self, request, pk):
        try:

            instance = get_object_or_404(ProcessedStock.objects.filter(id=pk))
            serializer_context = {
                'request': Request(request),
            }

            processed_stock_serializer = ProcessedStockSerializer(instance=instance, context=serializer_context)
            stock = instance.stock
            stock.processed = False
            stock.save()
            self.perform_destroy(instance)
            return Response(processed_stock_serializer.data, status=status.HTTP_200_OK)

        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProcessedStockArchive(APIView):
    """
        Archive list of Processed Stock ids.

    """

    @staticmethod
    def get_serializer():
        return ArchiveItemSerializer()

    def put(self, request, format=None):

        item_serializer = ArchiveItemSerializer(data=request.data)
        item_serializer.is_valid(raise_exception=True)
        archive_ids = item_serializer.data['archive_ids']
        content = {'archive_ids': archive_ids,
                   'status': 'OK'}
        for id_to_archive in archive_ids:
            for e in ProcessedStock.objects.filter(id=id_to_archive):
                e.archived = True
                e.save()
        return Response(content, status=status.HTTP_200_OK)
