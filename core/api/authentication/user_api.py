from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.request import Request
from core.serializers.user_serializer import UserSerializer
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        pk = self.kwargs.get('pk')

        if pk == "current":
            return self.request.user

        return super(UserViewSet, self).get_object()


class Logout(APIView):
    queryset = User.objects.all()

    def get(self, request, format=None):
        user = request.user

        serializer_context = {
            'request': Request(request),
        }

        user_serializer = UserSerializer(instance=user, context=serializer_context)

        request.user.auth_token.delete()
        return Response(user_serializer.data, status=status.HTTP_200_OK)
