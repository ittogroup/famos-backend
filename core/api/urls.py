import os

from rest_framework.authtoken import views

from core.api.farmos.bl_api import BLViewSet, BLCreateUpdate, PalletByBL
from core.api.farmos.city_api import CityViewSet
from core.api.farmos.client_api import ClientViewSet
from core.api.farmos.fcm_token_api import FcmTokenView
from core.api.farmos.packaging_api import PackagingViewSet
from core.api.farmos.pallet_api import PalletViewSet, PalletArchive, UpdatePalletClient
from core.api.farmos.parcel_api import ParcelViewSet
from core.api.farmos.processed_stock_api import ProcessedStockViewSet, ProcessedStockArchive
from core.api.farmos.products_api import ProductsViewSet
from core.api.farmos.reports.daily_reports import DailyReportsViewSet
from core.api.farmos.reports.general_reports import GeneralReportsViewSet
from core.api.farmos.variety_api import VarietyViewSet
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.documentation import include_docs_urls
from core.api.authentication.user_api import UserViewSet, Logout
from core.api.farmos.stock_api import StockViewSet, StockArchive, StocksBrutViewSet

router = routers.DefaultRouter()

router.register(r'users', UserViewSet)
router.register(r'packaging', PackagingViewSet)
router.register(r'stocks', StockViewSet)
router.register(r'varieties', VarietyViewSet)
router.register(r'clients', ClientViewSet)
router.register(r'parcels', ParcelViewSet)
router.register(r'cities', CityViewSet)
router.register(r'pallets', PalletViewSet)
router.register(r'processed_stocks', ProcessedStockViewSet)
router.register(r'products', ProductsViewSet)
router.register(r'daily_reports', DailyReportsViewSet,  base_name='daily_reports')
router.register(r'general_reports', GeneralReportsViewSet,  base_name='general_reports')
router.register(r'stocksBrut', StocksBrutViewSet,  base_name='stocksBrut')
router.register(r'bl', BLViewSet,  base_name='BL')

try:
    env = os.environ.get("FARM_OS_ENV")

    if env == 'PROD':
        from FarmOS.settings_prod import *
    else:
        from FarmOS.settings_dev import *
except ImportError:
    pass

urlpatterns = [
    url(r'^logout/?$', Logout.as_view()),
    url(r'^stocks/archive', StockArchive.as_view()),
    url(r'^processed_stocks/archive', ProcessedStockArchive.as_view()),
    url(r'^pallets/archive', PalletArchive.as_view()),
    url(r'^bl/create_update', BLCreateUpdate.as_view()),
    url(r'^bl/PalletByBL', PalletByBL.as_view()),
    url(r'^pallets/update_client', UpdatePalletClient.as_view()),
    url(r'^fcm/token', FcmTokenView.as_view()),
    url(r'^', include(router.urls)),
    url(r'^login/?$', views.obtain_auth_token),
]

if DEBUG:
    urlpatterns += [
        url(r'^docs/', include_docs_urls(title='FarmOs API')),
    ]
