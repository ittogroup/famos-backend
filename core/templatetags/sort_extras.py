from django import template
register = template.Library()


@register.filter
def sort_by(queryset, order_by):
    is_order_asc = False
    order_by_direction = order_by[0][0]
    if order_by_direction in ["-", "+"]:
        attr_to_order_by = str(order_by[1:])
        if order_by_direction == "-":
            is_order_asc = False
        elif order_by_direction == "+":
            is_order_asc = True
    else:
        attr_to_order_by = order_by

    return sorted(queryset, key=lambda x: int(x[1].__dict__[attr_to_order_by]), reverse=is_order_asc)
