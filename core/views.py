# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.shortcuts import render
from django.views.generic import TemplateView, DetailView

from core.models import Stock, BL, Pallet, PalletEntry

from .utils import PdfResponse


class PdfMixin(object):
    content_type = "application/pdf"
    response_class = PdfResponse


@login_required(login_url='/login')
def index(request):
    user = request.user

    return render(request, "index.html", {'user': user})


class UsersView(TemplateView):
    template_name = "views/users_view.html"


class VarietiesView(TemplateView):
    template_name = "views/varieties_view.html"


class ParcelsView(TemplateView):
    template_name = "views/parcels_view.html"


class ProductsView(TemplateView):
    template_name = "views/products_view.html"


class StocksView(TemplateView):
    template_name = "views/stocks_view.html"


class StockBrutView(TemplateView):
    template_name = "views/stocksbrut_view.html"


class ClientsView(TemplateView):
    template_name = "views/clients_view.html"


class StocksReportView(TemplateView):
    template_name = "views/reports/stocks_report_view.html"


class ProcessedReportView(TemplateView):
    template_name = "views/reports/processed_report_view.html"


class PalletsReportView(TemplateView):
    template_name = "views/reports/pallets_report_view.html"


class StockByVarietyReportsView(TemplateView):
    template_name = "views/reports/stock_by_variety_report_view.html"


class PackagingView(TemplateView):
    template_name = "views/packaging_view.html"


class BlView(TemplateView):
    model = BL
    template_name = "views/bl_view.html"
    
    def get_context_data(self, **kwargs):
        context = super(BlView, self).get_context_data(**kwargs)
        bls = BL.objects.filter(archived=True) #Change to false when you push in prod
        pallet_entries_data_dict = dict()
        # for bl in bls:
        #     pallets = Pallet.objects.filter(bl_id=bl.id)
        #     for pallet in pallets:
        #         pallet_entries = PalletEntry.objects.filter(pallet_id=pallet.id)
        #         for pallet_entry in pallet_entries:
        #             #define a key for every type of pallet_entry based on product, calibre and packaging
        #             key_pallet_entry = str(pallet.variety.parcel.product.id) + "_" + str(pallet_entry.calibre) + "_" + str(
        #                 pallet_entry.packaging.id)

        #             poid_net = pallet_entry.weight - (pallet_entry.units * pallet_entry.packaging.weight) - 18

        #             if pallet_entry.pallet.variety.name not in pallet_entries_data_dict:
        #                 pallet_entries_data_dict[pallet_entry.pallet.variety.name]= {
        #                     key_pallet_entry: pallet_entry
        #                 }
        #             else:
        #                 if key_pallet_entry not in pallet_entries_data_dict[pallet_entry.pallet.variety.name]
        #                     pallet_entries_data_dict[pallet_entry.pallet.variety.name][key_pallet_entry] = pallet_entry
        #                 else:
        #                     pallet_entries_data_dict[pallet_entry.pallet.variety.name][key_pallet_entry].units += pallet_entry.units
        #                     pallet_entries_data_dict[pallet_entry.pallet.variety.name][key_pallet_entry].poid_net += pallet_entry.poid_net
                    
        #             setattr(pallet_entry, "poid_net", poid_net)

        context['pallet_entries_data_dict'] = pallet_entries_data_dict
        return context








class BlPdfView(PdfMixin, DetailView):
    model = BL
    template_name = 'views/reports/bl_pdf_view.html'

    def get_context_data(self, **kwargs):
        context = super(BlPdfView, self).get_context_data(**kwargs)
        bl = kwargs['object']
        pallets = Pallet.objects.filter(bl_id=bl.id)
        pallets_data_dict = dict()
        total_units = 0
        total_poid_net = 0
        for pallet in pallets:
            pallet_entries = PalletEntry.objects.filter(pallet_id=pallet.id)
            for pallet_entry in pallet_entries:

                #define a key for every type of pallet_entry
                key_pallet_entry = str(pallet.variety.parcel.product.id) + "_" + str(pallet_entry.calibre) + "_" + str(
                    pallet_entry.packaging.id)

                poid_net = pallet_entry.weight - (pallet_entry.units * pallet_entry.packaging.weight) - 18

                product_name = pallet_entry.pallet.variety.parcel.product.name

                if product_name not in pallets_data_dict:
                    pallets_data_dict[product_name] = {
                        'pallets': {key_pallet_entry: pallet_entry},
                        'product': product_name
                        }
                else:
                    if key_pallet_entry not in pallets_data_dict[product_name]['pallets']:
                        pallets_data_dict[product_name]['pallets'][key_pallet_entry] = pallet_entry
                    else:
                        pallets_data_dict[product_name]['pallets'][
                            key_pallet_entry].units += pallet_entry.units
                        pallets_data_dict[product_name]['pallets'][
                            key_pallet_entry].poid_net += poid_net

                setattr(pallet_entry, "poid_net", poid_net)
                
                pallets_data_dict[product_name]["length"] = (
                    len(pallets_data_dict[product_name]['pallets']))

                print(pallets_data_dict)

                #update totals
                total_units = total_units + pallet_entry.units
                total_poid_net = total_poid_net + poid_net

        # pallets_data.append(pallets_data_dict)

        context['pallets_data_list'] = pallets_data_dict
        context['total_units'] = total_units
        context['total_poid_net'] = total_poid_net
        return context


class StockReportPrintView(TemplateView):
    template_name = "views/reports/printer/stocks_report_view.html"

    def get_context_data(self, *args, **kwargs):
        context = super(StockReportPrintView, self).get_context_data(*args, **kwargs)
        # date = self.request.GET["date"]

        # context['date'] = "12/2/2018"
        return context

