from rest_framework import serializers

from core.models import Packaging


class PackagingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Packaging
        fields = ('id', 'name', 'weight', 'packaging_type')
