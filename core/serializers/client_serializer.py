from rest_framework import serializers

from core.models import City, Client
from core.serializers.city_serializer import CitySerializer


class ClientSerializer(serializers.ModelSerializer):
    city = CitySerializer(
        read_only=True,
        required=False,
    )
    city_id = serializers.PrimaryKeyRelatedField(
        queryset=City.objects.all(),
        allow_null=False,
        write_only=True,
        required=False,
        source='city',
    )

    class Meta:
        model = Client
        fields = ('id', 'name', 'city', 'city_id', 'address', 'phone')
