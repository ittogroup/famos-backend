from django.utils.timezone import now
from rest_framework import serializers

from core.models import Pallet, PalletEntry, Client, Variety
from core.serializers.client_serializer import ClientSerializer
from core.serializers.pallet_entry_serializer import PalletEntrySerializer
from core.serializers.variety_serializer import VarietySerializer
from datetime import datetime

class TimestampField(serializers.DateTimeField):
    """
    Convert a django datetime to/from timestamp.
    """
    def to_representation(self, value):
        """
        Convert the field to its internal representation (aka timestamp)
        :param value: the DateTime value
        :return: a UTC timestamp integer
        """
        result = super(TimestampField, self).to_representation(value)
        return result

    def to_internal_value(self, value):
        """
        deserialize a timestamp to a DateTime value
        :param value: the timestamp value
        :return: a django DateTime value
        """
        tms = int(value / 1000)
        dt_object = datetime.fromtimestamp(tms)
        return super(TimestampField, self).to_representation(dt_object)


class PalletSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S%Z", required=False, read_only=True)
    processing_date = TimestampField(format="%Y-%m-%dT%H:%M:%S%Z", required=False, read_only=False)
    entries = serializers.SerializerMethodField(read_only=True)
    age = serializers.SerializerMethodField(read_only=True)

    destination = ClientSerializer(
        read_only=True,
        required=False,
    )

    variety = VarietySerializer(
        read_only=True,
        required=False,
    )

    variety_id = serializers.PrimaryKeyRelatedField(
        queryset=Variety.objects.all(),
        allow_null=False,
        write_only=True,
        required=True,
        source='variety',
    )

    destination_id = serializers.PrimaryKeyRelatedField(
        queryset=Client.objects.all(),
        allow_null=False,
        write_only=True,
        required=False,
        source='destination',
    )

    @staticmethod
    def get_age(obj):
        delta = (now() - obj.date)

        days = delta.days
        hours = (days * 24) + (delta.seconds / (60 * 60))
        return hours

    @staticmethod
    def get_entries(obj):
        try:
            pallets = PalletEntry.objects.filter(pallet_id=obj.id)
        except PalletEntry.DoesNotExist:
            return []
        return PalletEntrySerializer(pallets, many=True).data

    class Meta:
        model = Pallet
        fields = ('id', 'variety', 'date', 'date', 'processing_date', 'destination',
                  'entries', 'age', 'variety_id', 'destination_id', 'vrac')

