from rest_framework import serializers


class FcmTokenSerializer(serializers.Serializer):

    token = serializers.CharField(help_text="token")
    status = serializers.CharField(help_text="status", required=False)
    user_id = serializers.CharField(help_text="user id")

    class Meta:
        fields = ['token', 'user_id', 'status']


