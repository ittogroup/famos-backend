from rest_framework import serializers

from core.models import Variety, Parcel
from core.serializers.parcel_serializer import ParcelSerializer


class VarietySerializer(serializers.ModelSerializer):
    code = serializers.SerializerMethodField(read_only=True)

    parcel_id = serializers.PrimaryKeyRelatedField(
        queryset=Parcel.objects.all(),
        allow_null=False,
        write_only=True,
        required=True,
        source='parcel',
    )
    parcel = ParcelSerializer(
        read_only=True,
        required=False,
    )

    @staticmethod
    def get_code(obj):
        parcel = obj.parcel
        parcel_code = str(parcel.code)
        if parcel.code < 10:
            parcel_code = "0" + parcel_code

        variety_code = "A" + parcel.product.code + parcel_code + str(obj.code)

        return variety_code

    class Meta:
        model = Variety
        fields = ('id', 'name', 'code', 'parcel_id', 'parcel')
