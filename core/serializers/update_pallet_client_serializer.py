from rest_framework import serializers


class UpdatePalletClientSerializer(serializers.Serializer):

    pallet_id = serializers.IntegerField(help_text="An array of ids")
    client_id = serializers.IntegerField(help_text="An array of ids", required=False, allow_null=True)

    class Meta:
        fields = ['pallet_id', 'client_id']


