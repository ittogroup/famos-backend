from django.utils.timezone import now

from rest_framework import serializers

from core.models import Stock, Parcel, Variety, Packaging
from core.serializers.packaging_serializer import PackagingSerializer
from core.serializers.parcel_serializer import ParcelSerializer
from core.serializers.variety_serializer import VarietySerializer


class StockBrutSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S%Z", required=False, read_only=True)
    weight = serializers.DecimalField(required=True, max_digits=10, decimal_places=2)
    units = serializers.IntegerField(required=True)
    age = serializers.SerializerMethodField(read_only=True)

    variety_id = serializers.PrimaryKeyRelatedField(
        queryset=Variety.objects.all(),
        allow_null=False,
        write_only=True,
        required=True,
        source='variety',
    )
    variety = VarietySerializer(
        read_only=True,
        required=False,
    )

    packaging_id = serializers.PrimaryKeyRelatedField(
        queryset=Packaging.objects.all(),
        allow_null=False,
        write_only=True,
        required=True,
        source='packaging',
    )
    packaging = PackagingSerializer(
        read_only=True,
        required=False,
    )

    @staticmethod
    def get_age(obj):
        delta = (now() - obj.date)

        days = delta.days
        hours = (days * 24) + (delta.seconds / (60 * 60))
        return hours

    class Meta:
        model = Stock
        fields = ('id', 'variety', 'variety_id',
                  'weight', 'date', 'temporary', 'units', 'date', 'age', 'corporal', 'packaging', 'packaging_id','archived')





