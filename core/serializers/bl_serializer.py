from django.utils.timezone import now
from rest_framework import serializers

from core.models import Pallet, BL, Client
from core.serializers.client_serializer import ClientSerializer
from core.serializers.pallet_serializer import PalletSerializer


class BLSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S%Z", required=False, read_only=True)
    age = serializers.SerializerMethodField(read_only=True)
    client = ClientSerializer(
        read_only=True,
        required=False,
    )
    pallets = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_pallets(obj):
        try:
            pallets = []#Pallet.objects.filter(bl_id=obj.id)
        except Pallet.DoesNotExist:
            return []
        return PalletSerializer(pallets, many=True).data

    @staticmethod
    def get_age(obj):
        delta = (now() - obj.date)

        days = delta.days
        hours = (days * 24) + (delta.seconds / (60 * 60))
        return hours

    class Meta:
        model = BL
        fields = ('id', 'date', 'age', 'pallets', 'client')
