from rest_framework import serializers

from core.models import Parcel, Product
from core.serializers.product_serializer import ProductSerializer


class ParcelSerializer(serializers.ModelSerializer):
    code = serializers.IntegerField(read_only=True)

    product_id = serializers.PrimaryKeyRelatedField(
        queryset=Product.objects.all(),
        allow_null=False,
        write_only=True,
        required=True,
        source='product',
    )
    product = ProductSerializer(
        read_only=True,
        required=False,
    )

    class Meta:
        model = Parcel
        fields = ('id', 'name', 'product_id', 'product', 'code')
