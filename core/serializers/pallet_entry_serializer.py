from rest_framework import serializers

from core.models import Parcel, Variety, Pallet, PalletEntry, Packaging
from core.serializers.packaging_serializer import PackagingSerializer


class PalletEntrySerializer(serializers.ModelSerializer):
    weight = serializers.DecimalField(required=True, max_digits=10, decimal_places=2)
    weight_gross = serializers.DecimalField(required=True, max_digits=10, decimal_places=2)
    pallet_id = serializers.PrimaryKeyRelatedField(
        queryset=Pallet.objects.all(),
        allow_null=False,
        write_only=True,
        required=True,
        source='pallet',
    )

    packaging_id = serializers.PrimaryKeyRelatedField(
        queryset=Packaging.objects.all(),
        allow_null=False,
        write_only=True,
        required=True,
        source='packaging',
    )
    packaging = PackagingSerializer(
        read_only=True,
        required=False,
    )

    class Meta:
        model = PalletEntry
        fields = ('id', 'units', 'calibre', 'weight', 'weight_gross', 'pallet_id', 'packaging', 'packaging_id')
