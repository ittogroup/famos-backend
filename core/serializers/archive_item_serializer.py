from rest_framework import serializers


class ArchiveItemSerializer(serializers.Serializer):

    archive_ids = serializers.ListField(child=serializers.IntegerField(), help_text="An array of ids")

    class Meta:
        fields = ['archive_ids']


