from django.utils.timezone import now
from rest_framework import serializers

from core.models import Stock, ProcessedStock
from core.serializers.stock_seriliazer import StockSerializer


class ProcessedStockSerializer(serializers.ModelSerializer):

    date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S%Z", required=False, read_only=True)
    age = serializers.SerializerMethodField(read_only=True)

    stock_id = serializers.PrimaryKeyRelatedField(
        queryset=Stock.objects.all(),
        allow_null=False,
        write_only=True,
        required=True,
        source='stock',
    )

    stock = StockSerializer(
        read_only=True,
        required=False,
    )

    @staticmethod
    def get_age(obj):
        delta = (now() - obj.date)

        days = delta.days
        hours = (days * 24) + (delta.seconds / (60 * 60))
        return hours

    class Meta:
        model = ProcessedStock
        fields = ('id', 'date', 'stock', 'stock_id', 'age')


