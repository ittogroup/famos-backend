from django.contrib.auth.models import User
from django.utils.timezone import now
from rest_framework import serializers

from core.models import Update
from core.serializers.update_serializer import UpdateSerializer


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    email = serializers.CharField(required=False)
    id = serializers.IntegerField(read_only=True, source='pk', required=False)
    last_update = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_last_update(obj):
        update = Update.objects.all().first()
        if update is None:
            update = Update(clients=False, varieties=False, date=now())
        return UpdateSerializer(update, many=False).data

    class Meta:
        model = User
        fields = ('url', 'username', 'last_name', 'first_name', 'email', 'password', 'id', 'last_update')

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def validate(self, attrs):
        password = attrs['password']
        if len(password) < 4:
            raise serializers.ValidationError({"Error": "Password is too short."})
        return attrs
