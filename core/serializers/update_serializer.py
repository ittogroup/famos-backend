from rest_framework import serializers

from core.models import Variety, Parcel, Update
from core.serializers.parcel_serializer import ParcelSerializer


class UpdateSerializer(serializers.ModelSerializer):
    # date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S%Z", required=False, read_only=True)

    class Meta:
        model = Update
        fields = ('varieties', 'clients', 'packaging', 'date')
