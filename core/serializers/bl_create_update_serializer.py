from django.utils.timezone import now
from rest_framework import serializers

from core.models import Pallet, BL
from core.serializers.pallet_serializer import PalletSerializer


class BLCreateUpdateSerializer(serializers.ModelSerializer):

    client_id = serializers.IntegerField()
    pallets_ids = serializers.ListField(child=serializers.IntegerField(), help_text="An array of ids")

    class Meta:
        fields = ('client_id', 'pallets_ids')


