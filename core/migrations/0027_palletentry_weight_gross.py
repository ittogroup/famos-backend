# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-06-02 12:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0026_auto_20180307_2040'),
    ]

    operations = [
        migrations.AddField(
            model_name='palletentry',
            name='weight_gross',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
    ]
