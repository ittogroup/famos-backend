from django.conf.urls import url, include
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.views import login, logout

from FarmOS import settings
from core.views import index, UsersView, VarietiesView, ParcelsView, ProductsView, StocksView, ClientsView, \
    StocksReportView, ProcessedReportView, PalletsReportView, StockReportPrintView, StockByVarietyReportsView, \
    StockBrutView, PackagingView, BlView, BlPdfView

login_forbidden = user_passes_test(lambda u: u.is_anonymous(), '/', redirect_field_name=None)


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^login/$', login_forbidden(login), name="login", ),
    url(r'^logout/$', logout, {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^api/', include('core.api.urls')),
    url(r'^users/$', UsersView.as_view()),
    url(r'^packaging/$', PackagingView.as_view()),
    url(r'^varieties/$', VarietiesView.as_view()),
    url(r'^parcels/$', ParcelsView.as_view()),
    url(r'^products/$', ProductsView.as_view()),
    url(r'^stocks/$', StocksView.as_view()),
    url(r'^stocksBrut/$', StockBrutView.as_view()),
    url(r'^clients/$', ClientsView.as_view()),
    url(r'^stocksReports/$', StocksReportView.as_view()),
    url(r'^processedReports/$', ProcessedReportView.as_view()),
    url(r'^palletReports/$', PalletsReportView.as_view()),
    url(r'^stockPrintReport\/(.*)$', StockReportPrintView.as_view()),
    url(r'^StockByVarietyReports/$', StockByVarietyReportsView.as_view()),
    url(r'^bl/$', BlView.as_view()),
    url(r'^blPDF/(?P<pk>\d+)/$', BlPdfView.as_view()),
]
