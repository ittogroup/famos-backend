# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
from django.db import models


class Packaging(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, db_index=True, max_length=512)
    weight = models.DecimalField(max_digits=10, decimal_places=2)
    packaging_type = models.CharField(db_index=True, null=True, max_length=3)


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, db_index=True, max_length=512)
    code = models.CharField(db_index=True, max_length=6, default="")


class Parcel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(db_index=True, max_length=512)
    code = models.IntegerField(default=0)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, default=None, null=True)

    def save(self, *args, **kwargs):
        if self.code == 0:
            self.code = self.product.parcel_set.count() + 1
        else:
            self.code = 1

        super(Parcel, self).save(*args, **kwargs)


class City(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, db_index=True, max_length=512)


class Variety(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(db_index=True, max_length=512)
    parcel = models.ForeignKey(Parcel, default=None)
    code = models.IntegerField(default=0)
    color = models.CharField(max_length=512, default='')

    def save(self, *args, **kwargs):
        if self.code == 0:
            self.code = self.parcel.variety_set.count() + 1
        else:
            self.code = 1

        super(Variety, self).save(*args, **kwargs)


class Stock(models.Model):
    id = models.AutoField(primary_key=True)
    variety = models.ForeignKey(Variety)
    packaging = models.ForeignKey(Packaging, null=True, default=None)
    units = models.IntegerField(default=0)
    weight = models.DecimalField(max_digits=10, decimal_places=2)
    temporary = models.BooleanField(default=False)
    processed = models.BooleanField(default=False)
    archived = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    corporal = models.CharField(db_index=True, max_length=512, default="")


class Update(models.Model):
    id = models.AutoField(primary_key=True)
    varieties = models.BooleanField(default=False)
    clients = models.BooleanField(default=False)
    packaging = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True, blank=True)


class City(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, db_index=True, max_length=512)


class Client(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(db_index=True, max_length=512)
    phone = models.CharField(db_index=True, max_length=512, default=None, null=True)
    address = models.CharField(db_index=True, max_length=1024, default=None, null=True)
    city = models.ForeignKey(City, null=True, default=None)


class BL(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    client = models.ForeignKey(Client, null=True, default=None)
    archived = models.BooleanField(default=False)


class Pallet(models.Model):
    id = models.AutoField(primary_key=True)
    variety = models.ForeignKey(Variety, null=False, default=None)
    destination = models.ForeignKey(Client, null=True, default=None)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    processing_date = models.DateTimeField(blank=True, null=True, default=None)
    archived = models.BooleanField(default=False)
    vrac = models.BooleanField(default=False)
    bl = models.ForeignKey(BL, default=None, null=True)


class PalletEntry(models.Model):
    id = models.AutoField(primary_key=True)
    units = models.IntegerField(default=0)
    calibre = models.IntegerField(default=0)
    pallet = models.ForeignKey(Pallet, on_delete=models.CASCADE, default=None, null=True)
    weight = models.DecimalField(max_digits=10, decimal_places=2)
    weight_gross = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    packaging = models.ForeignKey(Packaging, null=True, default=None)
    archived = models.BooleanField(default=False)


class ProcessedStock(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    stock = models.ForeignKey(Stock)
    archived = models.BooleanField(default=False)


class FcmToken(models.Model):
    id = models.AutoField(primary_key=True)
    token = models.CharField(db_index=True, max_length=512)
    user_id = models.CharField(db_index=True, max_length=512)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
